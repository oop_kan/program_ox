import java.util.Scanner;

class ProgramOX {
    Scanner scanner = new Scanner(System.in);
    private String board[][];
    private String turn = " ";
    private String  winner = "-";

    public ProgramOX(){
        board = new String[3][3];
        for (int i = 0 ; i < 3 ; i++){
            for (int l = 0 ; l < 3 ; l++){
                board[i][l] = "-";
            }
        }
    }
    
    public void playGame(){
        printWelcome();
        displayBoard();
        setFirstTurn();
        while (true) {
            inputPoint();
            displayBoard();

            if (checkWin()){
                break;
            }else if(checkDraw()){
                System.out.println("DRAWWW!!!");
                break;
            }
        }
    }

    public void printWelcome(){
        System.out.println("Welcome to OX Game");
    }

    public boolean checkDraw(){
        if(countRound() == 10){
            return true;
        } else {
            return false;
        }
    }

    public void setTurn(String turn) {
        this.turn = turn;
    }

    public String[][] getBoard() {
        return board;
    }

    public void displayBoard(){
        for (int i = 0 ; i < board.length ; i++){
            for (int l = 0 ; l < board.length ; l++){
                System.out.print(board[i][l] + " ");
            }
            System.out.println();
        }
    }

    public void setBoard(int rowPoint, int colPoint) {
        board[rowPoint][colPoint] = turn;
        if (turn == "O"){
            turn = "X";
        } else {
            turn = "O";
        }
    }
       
    public boolean selectPointOnBoard(int rowPoint, int colPoint){
        if (rowPoint >= board.length || colPoint >= board.length){
            System.out.println("You are input row or col out of board?\n Please try again");
            inputPoint();
            return false;
        } else if (board[rowPoint][colPoint] != "-") {
            System.out.println("There are already have Symbol on board\n Please try again");
            inputPoint();
            return false;
        } else {
            return true;
        }
    }

    

    public void inputPoint(){
        int round = countRound();
        System.out.println("Round = " + round + " Turn = " + turn);
        System.out.print("RowPoint = ");
        int rowPoint = scanner.nextInt() - 1;
        System.out.print("ColPoint = ");
        int colPoint = scanner.nextInt() - 1;
        
        if (selectPointOnBoard(rowPoint, colPoint)){
            setBoard(rowPoint, colPoint);
        }
    }

    public void setFirstTurn(){
        System.out.print("Firstturn = ");
        turn = scanner.nextLine();
    }

    public int countRound(){
        int count = 0 ;
        for(int i = 0 ; i < board.length ; i++){
            for(int l = 0 ; l < board.length ; l++){
                if (board[i][l] != "-"){
                    count++;
                }
            }
        }
        return count+1;
    }

    
    
    public boolean checkWin(){
        boolean eventA = (board[0][0] == board[0][1] && board[0][0] == board[0][2]);
        boolean eventB = (board[0][0] == board[1][1] && board[0][0] == board[2][2]);   
        boolean eventC = (board[0][0] == board[1][0] && board[0][0] == board[2][0]);
        boolean eventD = (board[2][0] == board[2][1] && board[2][0] == board[2][2]);
        boolean eventE = (board[0][2] == board[1][2] && board[0][2] == board[2][2]);
        boolean eventF = (board[0][2] == board[1][1] && board[0][2] == board[2][0]);
        boolean eventG = (board[0][1] == board[1][1] && board[0][1] == board[2][1]);
        boolean eventH = (board[1][0] == board[1][1] && board[1][0] == board[1][2]);
        boolean[] event = {eventA,eventB,eventC,eventD,eventE,eventF,eventG,eventH};
        int i;
        for ( i = 0 ; i < event.length ; i++){
            if (event[i]){
                break;
            }
        }
        if (i != event.length) {
            if (i == 0 || i == 2){
                winner = board[0][0];
            } else if (i == 3 || i == 4){
                winner = board[2][2];
            } else {
                winner = board[1][1];
            }
            if (winner == "-"){
                return false;
            } 
            System.out.println(" Winner = Player " +  winner);
            return true;
        } else {
            return false;
        }
    }
}


public class ProgramOXApp {
        public static void main(String[] args) {
            
            ProgramOX programOX = new ProgramOX();
            programOX.playGame();
    }
}